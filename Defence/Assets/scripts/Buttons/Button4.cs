using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.Image;

public class Button4 : MonoBehaviour
{

    public GameObject Door;
    Animator myAnim;
    public bool Closed;
    

    public AudioSource CloseSound;
    public AudioSource OpenSound;
    public AudioClip CloseDoor;
    public AudioClip OpenDoor;

    // Start is called before the first frame update
    void Start()
    {
        myAnim = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Hand")
        {
            if (Closed == false)
            {
              Closed = true;
              Debug.Log("Detect Close 2");
              Door4.Instance.CloseTheDoor();
              CloseSound.Play();
            }

            if (other.gameObject.tag == "Hand")
                if (Closed == true)
            {
                Closed = false;
                Debug.Log("Detect Open 2");
                Door4.Instance.OpenTheDoor();
                OpenSound.Play();
                }
 
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}





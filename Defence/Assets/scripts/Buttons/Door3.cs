using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door3 : MonoBehaviour
{

    public static Door3 Instance { get; set; }
    Animator BamBamBamBam;

    // Start is called before the first frame update
    void Start()
    {
        BamBamBamBam = GetComponent<Animator>();
    }

    public void Awake()
    {
        Instance = this;
    }

    public void CloseTheDoor()
    {
        Debug.Log("Dzhekpot");
        BamBamBamBam.Play("Close1");

    }
    public void OpenTheDoor()
    {
        Debug.Log("OpenTheDoor");
        BamBamBamBam.Play("open1");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

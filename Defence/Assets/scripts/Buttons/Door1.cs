using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door1 : MonoBehaviour
{

    public static Door1 Instance { get; set; }
    Animator BamBamBamBam;

    // Start is called before the first frame update
    void Start()
    {
        BamBamBamBam = GetComponent<Animator>();
    }

    public void Awake()
    {
        Instance = this;
    }

    public void CloseTheDoor()
    {
        Debug.Log("Dzhekpot");
        BamBamBamBam.Play("Door1OpenAnim");

    }
    public void OpenTheDoor()
    {
        Debug.Log("OpenTheDoor");
        BamBamBamBam.Play("Door1REALOpenAnim");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door2 : MonoBehaviour
{

    public static Door2 Instance { get; set; }
    Animator BamBamBamBam;

    // Start is called before the first frame update
    void Start()
    {
        BamBamBamBam = GetComponent<Animator>();
    }

    public void Awake()
    {
        Instance = this;
    }

    public void CloseTheDoor()
    {
        Debug.Log("Dzhekpot");
        BamBamBamBam.Play("close");

    }
    public void OpenTheDoor()
    {
        Debug.Log("OpenTheDoor");
        BamBamBamBam.Play("open");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door4 : MonoBehaviour
{

    public static Door4 Instance { get; set; }
    Animator BamBamBamBam;

    public AudioSource CloseSound;
    public AudioSource OpenSound;
    public AudioClip CloseSoundTest;
    public AudioClip OpenSoundTest;

    // Start is called before the first frame update
    void Start()
    {
        BamBamBamBam = GetComponent<Animator>();
    }

    public void Awake()
    {
        Instance = this;
    }

    public void CloseTheDoor()
    {
        Debug.Log("Dzhekpot");
        BamBamBamBam.Play("CloseDoor4");
       

    }
    public void OpenTheDoor()
    {
        Debug.Log("OpenTheDoor");
        BamBamBamBam.Play("OpenDoor4");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TESTSCRIPT : MonoBehaviour
{

    public AudioSource Ambient1;
    public AudioSource Ambient2;
    public AudioSource PowerOut;
    public AudioSource Radio;

    public GameObject Light1;
    public GameObject Light2;
    public GameObject Light3;
    public GameObject Light4;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartNight());
    }

    // Update is called once per frame
    public IEnumerator StartNight()
    {
        // Door1.Instance.CloseTheDoor();
       // Door1.Instance.CloseTheDoor();
       // Door2.Instance.CloseTheDoor();
       // Door3.Instance.CloseTheDoor();
       // Door4.Instance.CloseTheDoor();

        int randomtime1 = Random.Range(12, 20);
        yield return new WaitForSeconds(randomtime1);
        Radio.Stop();
        PowerOut.Play();
        Light1.SetActive(false);
        Light2.SetActive(false);
        Light3.SetActive(false);
        Light4.SetActive(false);
        // Door1.Instance.OpenTheDoor();
        // Door2.Instance.OpenTheDoor();
        //  Door3.Instance.OpenTheDoor();
        // Door4.Instance.OpenTheDoor();
        int randomtime2 = Random.Range(6, 10);
        yield return new WaitForSeconds(randomtime2);
        Ambient1.Play();
        Ambient2.Play();
        Debug.Log("Start night");

    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class timer : MonoBehaviour
{
    public float hp = 100;
    public TMP_Text hpText;
    void Start()
    {
        hpText.text = ((int)hp).ToString();
    }

    void UpdateHpText()
    {
        hpText.text = ((int)hp).ToString();
    }
   
    void FixedUpdate()
    {

        UpdateHpText();
        hp -= Time.deltaTime;
        if (hp <= 1)
        {
            SceneManager.LoadScene("WIN");
        }
    }
}
   
   
